package com.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.dao.AlternativaDAO;
import com.dao.CuestionarioDAO;
import com.dao.PreguntaDAO;
import com.modelo.Alternative;
import com.modelo.Question;
import com.modelo.Questionnaire;
 
@Path("/xmlcuestionario")
public class XmlCuestionario {
    @GET
    @Produces("application/xml")
    public String recoveryCuestionary() {
 
        Integer IDcuestionario = 1;
        String title = "";
        Integer np = 0; //n preguntas
        Integer na = 0; //n alternativas
        
        String XmlArbol;
        String XmlId;
        String XmlPreguntas;
        String XmlPreguntaArray="";
        String XmlTitle;
        String XmlAlternativas;
        String XmlAlternativaArray="";
        String XmlCorrecta = "";
        
        CuestionarioDAO cuestionarioDao = new CuestionarioDAO();
        try {
			title = cuestionarioDao.getTitulo(IDcuestionario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Cargarcuestionario(IDcuestionario);//todo ta guardado en preguntas.
        
        
        XmlId = "<id>" + IDcuestionario + "</id>";
        XmlTitle = "<titulo>" + title + "</titulo>";
        
        np = preguntas.size();
        for(Integer i=0;i<=np-1;i++){
        	na = preguntas.get(i).getAlternativas().size();
        	XmlAlternativaArray="";
        	for(Integer j=0;j<=na-1;j++){
        		XmlAlternativaArray += "<alternativa_" + (j+1) +">" + preguntas.get(i).getAlternativas().get(j).getDescription() +"</alternativa_" + (j+1) +">";
        	}
        	XmlAlternativas = "<alternativas>" + XmlAlternativaArray + "</alternativas>";
        	XmlCorrecta = "<correcta>" + preguntas.get(i).getAlternativecorrect() + "</correcta>";
        	XmlPreguntaArray += "<pregunta_" + (i+1) +">" + "<pregunta>" + preguntas.get(i).getQuestion() +  "</pregunta>" + XmlAlternativas + XmlCorrecta + "</pregunta_" + (i+1) +">";
        }
        
        XmlPreguntas = "<preguntas>" + XmlPreguntaArray + "</preguntas>";
        XmlArbol = "<cuestionario>" + XmlId + XmlTitle + XmlPreguntas + "</cuestionario>";
        
        return XmlArbol;
    }
 
    @Path("{id}")
    @GET
    @Produces("application/xml")
    public String recoveryCuestionaryfromInput(@PathParam("id") Integer id) {
 
    	Integer IDcuestionario = id;
    	String title = "";
        Integer np = 0; //n preguntas
        Integer na = 0; //n alternativas
        
        String XmlArbol;
        String XmlId;
        String XmlPreguntas;
        String XmlPreguntaArray="";
        String XmlTitle;
        String XmlAlternativas;
        String XmlAlternativaArray="";
        String XmlCorrecta = "";
        
        CuestionarioDAO cuestionarioDao = new CuestionarioDAO();
        try {
			title = cuestionarioDao.getTitulo(IDcuestionario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Cargarcuestionario(IDcuestionario);//todo ta guardado en preguntas.
        
        
        XmlId = "<id>" + IDcuestionario + "</id>";
        XmlTitle = "<titulo>" + title + "</titulo>";
        
        np = preguntas.size();
        for(Integer i=0;i<=np-1;i++){
        	na = preguntas.get(i).getAlternativas().size();
        	XmlAlternativaArray="";
        	for(Integer j=0;j<=na-1;j++){
        		XmlAlternativaArray += "<alternativa_" + (j+1) +">" + preguntas.get(i).getAlternativas().get(j).getDescription() +"</alternativa_" + (j+1) +">";
        	}
        	XmlAlternativas = "<alternativas>" + XmlAlternativaArray + "</alternativas>";
        	XmlCorrecta = "<correcta>" + preguntas.get(i).getAlternativecorrect() + "</correcta>";
        	XmlPreguntaArray += "<pregunta_" + (i+1) +">" + "<pregunta>" + preguntas.get(i).getQuestion() +  "</pregunta>" + XmlAlternativas + XmlCorrecta + "</pregunta_" + (i+1) +">";
        }
        
        XmlPreguntas = "<preguntas>" + XmlPreguntaArray + "</preguntas>";
        XmlArbol = "<cuestionario>" + XmlId + XmlTitle + XmlPreguntas + "</cuestionario>";
        
               
        //return "<ctofservice>" + "<celsius>" + IDcuestionario + "</celsius>" + "<ctofoutput>" + result + "</ctofoutput>" + "</ctofservice>";
        return XmlArbol;
    }
    
    private Questionnaire cu = new Questionnaire();//para crear el cuestionario
	private Question qt = new Question();//para crear una pregunta
    private PreguntaDAO preDAO = new PreguntaDAO();
	private AlternativaDAO altDAO = new AlternativaDAO();
	private List<Question> preguntas = new ArrayList<Question>(); //definimos la lista de cuestionarios
	private Integer Ida = 0;
	
    public void Cargarcuestionario(Integer IDcuestionario){
		
		//cargamos las preguntas
		Cargarpreguntas(IDcuestionario);
		
		//cargamos a cada pregunta sus alternativas
		Integer n = 0;
		List<Alternative> arrayalt;
		Integer idq = 0; 
		
		n = preguntas.size();
		for(Integer i=0;i < n ; i++){
			idq = preguntas.get(i).getId();
			arrayalt = Cargaralternativas(idq.toString());
			preguntas.get(i).setAlternativas(arrayalt);
			
		}
	}
    
    public void Cargarpreguntas(Integer ID){

		try {
			cu.setId(ID);
			
			preguntas = preDAO.Preguntas(cu);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}  
    
    public List<Alternative> Cargaralternativas(String ids){

		List<Alternative> lal = new ArrayList<Alternative>();
		
		try {
			Ida = Integer.parseInt(ids);
			qt.setId(Ida);
			
			lal = altDAO.Alternativas(qt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lal;
		
	}
    
}