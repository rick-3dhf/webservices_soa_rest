package com.beans;

import com.objetos.Cuestionary;


//import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.ManagedProperty;
//import javax.faces.bean.ViewScoped;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.hibernate.Cuestionario;
import com.dao.CuestionarioDAO;
import com.modelo.Questionnaire;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class ShowQL {
		
	private String user = "0"; // nombre delusuario
	
	private CuestionarioDAO cueDAO = new CuestionarioDAO();
	
	private List<Questionnaire> lista = new ArrayList<Questionnaire>(); //definimos la lista de cuestionarios
	
	public void setUser(String iuser){

		this.user=iuser;
	}
	
	public String getUser(){
		
		return user;
	}
	
	public List<Questionnaire> getLista(){
		return lista;
	}
	
	public void setLista(List<Questionnaire> lista) {
		this.lista = lista;
	}
	
	//recuperar los datos de la consulta
	public void Listar(){

		BasicConfigurator.configure();
        Logger log = Logger.getLogger("Logger de Ejemplo");
        log.debug("Listando");
		
		try {
			lista = cueDAO.Listar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
