package com.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
 



import NET.webserviceX.www.UsdToPen;

import java.io.Serializable;
import java.rmi.RemoteException;
 
@ManagedBean
@RequestScoped
public class PremiumBean implements Serializable {
 
	private static final long serialVersionUID = 1L;
 
	private Double Monto;
	private String divisa = "USD";
	
	private boolean check1;
	private boolean check2;
	private boolean check3;
	private boolean check4;
	private boolean check5;
	
	private UsdToPen rate= new UsdToPen();
	
	public boolean getCheck1() {
		return check1;
	}
	public void setCheck1(boolean check1) {
		this.check1 = check1;
	}
	public boolean getCheck2() {
		return check2;
	}
	public void setCheck2(boolean check2) {
		this.check2 = check2;
	}
	public boolean getCheck3() {
		return check3;
	}
	public void setCheck3(boolean check3) {
		this.check3 = check3;
	}
	public boolean getCheck4() {
		return check4;
	}
	public void setCheck4(boolean check4) {
		this.check4 = check4;
	}
	public boolean getCheck5() {
		return check5;
	}
	public void setCheck5(boolean check5) {
		this.check5 = check5;
	}

	public String getDivisa() {
		return divisa;
	}
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}
	
	public String getCalcularMonto(){
		Double Item1;
		Double Item2;
		Double Item3;
		Double Item4;
		Double Item5;
		String Moneda = "";
		
		if(check1==true){
			Item1 = 10.0;
		}
		else{
			Item1 = 0.0;
		}
		
		if(check2==true){
			Item2 = 10.0;
		}
		else{
			Item2 = 0.0;
		}
		
		if(check3==true){
			Item3 = 10.0;
		}
		else{
			Item3 = 0.0;
		}
		
		if(check4==true){
			Item4 = 10.0;
		}
		else{
			Item4 = 0.0;
		}
		
		if(check5==true){
			Item5 = 10.0;
		}
		else{
			Item5 = 0.0;
		}
		
		Monto = Item1 + Item2 + Item3 + Item4 + Item5;
		if(divisa.equals("USD")){
			Moneda = "$ ";
			//no hacemos nada 
		}
		else if(divisa.equals("PEN")){
			Moneda = "S/. ";
			try {
				Monto = Monto * rate.RateUSDtoPEN();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		
		return "COSTO TOTAL: " + Moneda + String.format(String.valueOf(Math.rint(Monto*100)/100),"%.2f %n");
	}	

}