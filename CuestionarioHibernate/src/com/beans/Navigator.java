package com.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.*;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.mindmap.DefaultMindmapNode;
import org.primefaces.model.mindmap.MindmapNode;

@ManagedBean
public class Navigator {

	private String[] results = { "uno", "dos", "tres" };
	private String texto;
	private Date date;
	private MindmapNode root;

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date dt) {
		this.date = dt;
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String tx) {
		this.texto = tx;
	}

	public String escoger() {
		return results[2];

	}

	public List<String> complete(String query) {
		List<String> results = new ArrayList<String>();
		for (int i = 0; i < 10; i++)
			results.add(query + i);
		return results;
	}

	public void MindmapBean() {
		root = new DefaultMindmapNode("../", "Google", "FFCC00", false);

		MindmapNode ips = new DefaultMindmapNode("IPs", "IP Nos", "6e9ebf",
				true);
		MindmapNode ns = new DefaultMindmapNode("NS(s)", "Names", "6e9ebf",
				true);
		MindmapNode mw = new DefaultMindmapNode("Mw", "Malicious ", "6e9ebf",
				true);

		root.addNode(ips);
		root.addNode(ns);
		root.addNode(mw);
	}

	public MindmapNode getRoot() {
		return root;
	}

	public void onNodeSelect(SelectEvent event) {
		MindmapNode node = (MindmapNode) event.getObject();
		// load children of select node and add via node.addNode(childNode);
	}

}
