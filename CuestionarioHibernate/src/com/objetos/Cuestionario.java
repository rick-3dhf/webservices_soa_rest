package com.objetos;

public class Cuestionario {
	public String nro; 
	public String titulo;
	public String preguntas;
	
	public String getNro() { 
		return nro; 
		} 
	public void setNro(String nro) { 
		this.nro = nro; 
		} 
	public String getTitulo() { 
		return titulo; 
		} 
	public void setTitulo(String titulo) { 
		this.titulo = titulo; 
		} 
	public String getPreguntas() { 
		return preguntas; 
		} 
	public void setPreguntas(String preguntas) { 
		this.preguntas = preguntas; 
		} 
}
