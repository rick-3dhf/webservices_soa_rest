package com.home;

// Generated 14/07/2014 08:12:34 AM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.modelo.Enrolmenttype;

/**
 * Home object for domain model class Enrolmenttype.
 * @see com.modelo.Enrolmenttype
 * @author Hibernate Tools
 */
public class EnrolmenttypeHome {

	private static final Log log = LogFactory.getLog(EnrolmenttypeHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext()
					.lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Enrolmenttype transientInstance) {
		log.debug("persisting Enrolmenttype instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Enrolmenttype instance) {
		log.debug("attaching dirty Enrolmenttype instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Enrolmenttype instance) {
		log.debug("attaching clean Enrolmenttype instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Enrolmenttype persistentInstance) {
		log.debug("deleting Enrolmenttype instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Enrolmenttype merge(Enrolmenttype detachedInstance) {
		log.debug("merging Enrolmenttype instance");
		try {
			Enrolmenttype result = (Enrolmenttype) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Enrolmenttype findById(int id) {
		log.debug("getting Enrolmenttype instance with id: " + id);
		try {
			Enrolmenttype instance = (Enrolmenttype) sessionFactory
					.getCurrentSession().get("com.modelo.Enrolmenttype", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Enrolmenttype instance) {
		log.debug("finding Enrolmenttype instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("com.modelo.Enrolmenttype")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
