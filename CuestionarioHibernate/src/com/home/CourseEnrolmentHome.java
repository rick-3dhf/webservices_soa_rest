package com.home;

// Generated 14/07/2014 08:12:34 AM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.modelo.CourseEnrolment;

/**
 * Home object for domain model class CourseEnrolment.
 * @see com.modelo.CourseEnrolment
 * @author Hibernate Tools
 */
public class CourseEnrolmentHome {

	private static final Log log = LogFactory.getLog(CourseEnrolmentHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext()
					.lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(CourseEnrolment transientInstance) {
		log.debug("persisting CourseEnrolment instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(CourseEnrolment instance) {
		log.debug("attaching dirty CourseEnrolment instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(CourseEnrolment instance) {
		log.debug("attaching clean CourseEnrolment instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(CourseEnrolment persistentInstance) {
		log.debug("deleting CourseEnrolment instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public CourseEnrolment merge(CourseEnrolment detachedInstance) {
		log.debug("merging CourseEnrolment instance");
		try {
			CourseEnrolment result = (CourseEnrolment) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public CourseEnrolment findById(int id) {
		log.debug("getting CourseEnrolment instance with id: " + id);
		try {
			CourseEnrolment instance = (CourseEnrolment) sessionFactory
					.getCurrentSession().get("com.modelo.CourseEnrolment", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(CourseEnrolment instance) {
		log.debug("finding CourseEnrolment instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("com.modelo.CourseEnrolment")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
