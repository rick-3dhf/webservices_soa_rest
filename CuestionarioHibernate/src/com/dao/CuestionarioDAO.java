package com.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.primefaces.component.log.Log;

import com.funciones.HibernateUtil;
import com.modelo.Questionnaire;

import java.util.List;
import java.util.ArrayList;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class CuestionarioDAO {
	
	private Session session;

	public List<Questionnaire> Listar() throws Exception{
		
		List<Questionnaire> lq = new ArrayList<Questionnaire>();
		Questionnaire qu = null;
		Integer n = 0;
		
		try{
			
			session = HibernateUtil.getSessionfactory().openSession();
			String hql = "FROM Questionnaire";
			
			Query query = session.createQuery(hql);
			
			if(!query.list().isEmpty()){
				
				n=query.list().size();
				BasicConfigurator.configure();
		        Logger log = Logger.getLogger("Logger de Ejemplo");
				for(Integer i=0; i<= n-1; i++)
				{
					qu = (Questionnaire) query.list().get(i);
					lq.add(qu);
			        
			        log.debug(i);
			        
				}
				
			}
			
		}catch(Exception e){
			
			throw e;
			
		}
		return lq;
	}
	
	public String getTitulo(Integer Id) throws Exception{
		
		List<Questionnaire> lq = new ArrayList<Questionnaire>();
		Questionnaire qu = null;
		Integer n = 0;
		String titulo = "";
		
		try{
			
			session = HibernateUtil.getSessionfactory().openSession();
			String hql = "FROM Questionnaire WHERE id = '" + Id + "'";
			
			Query query = session.createQuery(hql);
			
			if(!query.list().isEmpty()){
				
				n=query.list().size();
				BasicConfigurator.configure();
		        Logger log = Logger.getLogger("Logger de Ejemplo");
				for(Integer i=0; i<= n-1; i++)
				{
					qu = (Questionnaire) query.list().get(i);
					titulo = qu.getTitle(); 
				}
				
			}
			
		}catch(Exception e){
			
			throw e;
			
		}
		return titulo;
	}
	
}


