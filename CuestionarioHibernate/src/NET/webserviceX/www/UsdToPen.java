package NET.webserviceX.www;

import java.rmi.RemoteException;
import NET.webserviceX.www.*;

public class UsdToPen {
	
	public  Double RateUSDtoPEN() throws RemoteException{
		Currency PE = new Currency("USD");
		Currency US = new Currency("PEN");	
		double rate = 0;
		CurrencyConvertorSoapProxy conversor = new CurrencyConvertorSoapProxy();
		rate = conversor.conversionRate(PE,US);
		return rate;
	}
	
}
